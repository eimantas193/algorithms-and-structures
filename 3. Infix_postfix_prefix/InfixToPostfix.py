from pythonds.basic import Stack # iš pythonds bibliotekos, basic skripto įkeliama klasė Stack

def infixToPostfix(infixexpr): # funkcija su vienu kintamuoju į kurią paduodama eilutė
    prec = {} # sukuriamas žodynas
    prec["*"] = 3 # priskiriama daugybai 3
    prec["/"] = 3 # priskiriama dalybai 3
    prec["+"] = 2 # priskiriama sudėčiai 3
    prec["-"] = 2 # priskiriama atimčiai 3
    prec["("] = 1 # priskiriama skliaustui 3
    opStack = Stack() # opStack priskiriama Stack klasė su jos funkcijom
    postfixList = [] # sukuriamas tuščias sąrašas prstfixList
    tokenList = infixexpr.split() # išskaidomos infixexpr į sąrašą su kiekveinam ženklui priskirtu indeksu

    for token in tokenList: # einama per visus tokenList elementus
        if token in "ABCDEFGHIJKLMNOPQRSTUVWXYZ" or token in "0123456789":  # tikrinama ar esamas elementas yra raidė arba skaičius
            postfixList.append(token) # jei atitinka sąlygą viršuje pridedamas token elementas prie postfixList sąrašo pabaigoje 
        elif token == '(':  # jei elementas yra '(' įeinama į elif sąlygą
            opStack.push(token) # elementas pridedamas prie Stack klasės sąrašo
        elif token == ')': # jei elementas yra ')' įeinama į elif sąlygą
            topToken = opStack.pop() # iš opStack sąrašo išimamas paskutinis elemetas ir priskiriamas topToken
            while topToken != '(': # ciklas sukasi iki randamas '(' elementas
                postfixList.append(topToken)  # iki galioje sąlyga viršuje topToken esamas elementas pridedamas paskutinis prie postficList sąrašo 
                topToken = opStack.pop() # iš Stack klasės sąrašo išimamas paskutinis elemetas ir priskiriamas topToken
        else: # jei nei viena sąlyga netinka vygdomos apačioje esamos funkcijos
            while (not opStack.isEmpty())/ #ciklas sukasi iki atitinka dvi salygas pirma opStack nėra tuščias  
             and (prec[opStack.peek()] >= prec[token]):  #ir tikrinamos priskirtos reikšmės yra didesnė arba už token reikšmė  
                  postfixList.append(opStack.pop())  # jei viršuje sąlyga išpildoma elementas pridedamas paskutinis prie postficList sąrašo 
            opStack.push(token) # jei neatitinka jokios salygos elemetas pridedamas prie opStack sąrašo kaip paskutinis elementas

    while not opStack.isEmpty(): # ciklas vyksta iki opStack nėra tuščias
        postfixList.append(opStack.pop()) # prie postficList sąrašo iš galo pridedama po vieną opStack elementą kurie irgi imami iš galo 
    return " ".join(postfixList)  # postfixList sąrašo elemnatai sujungiami į vieną eilutę (string)

print(infixToPostfix("A * B + C * D")) # siunčiama eilutė į infixToPostfix funkciją ir rezultatą išvedama į komados eilutę
print(infixToPostfix("( A + B ) * C - ( D - E ) * ( F + G )")) # siunčiama eilutė į infixToPostfix funkciją ir rezultatą išvedama į komados eilutę