class Stack: # klasė pavadinimu Stack
	     def __init__(self):# inicijuojama pagrindinė funkcija
	         self.items = []# priskiriamas sąrašas self funkcijai
	
	     def isEmpty(self):# funkcija isEmpty() ima tik vieną argumentą self
	         return self.items == []# gražinama True jei sąrašas tuščias kitu atveju False
	
	     def push(self, item): # funkcija push() ima argumentą self ir gautą reikšmę iš kreipimosi
	         self.items.insert(0,item) # duotasis elemetas yra įterpaimas į sąrašo pradžią pakialiant visų likusių elementų indeksus per vienetą
	
	     def pop(self): # funkcija pop() ima tik vieną argumentą self
	         return self.items.pop(0) # iš sąrašo pašalina pirmajį narį ir gražina jo reikšmę ir sumažina visų likusių elemetų indeksą per vienetą
	
	     def peek(self): # funkcija peek() ima tik vieną argumentą self 
	         return self.items[0] # iš sąrašo grąžina pirmą reikšmę
	
	     def size(self): # funkcija size() ima tik vieną argumentą self 
	         return len(self.items) # gražina sąrašo dydį

s = Stack()
s.push('hello') # kreipiasi į funkciją push() ir siunčia eilutę 'hello'
s.push('true') # kreipiasi į funkciją push() ir siunčia eilutę 'true'
print(s.pop())  # kreipiasi į funkciją pop() ir išveda gautą rezultatą į ekraną

