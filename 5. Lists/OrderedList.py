class Node: # klasė pavadinimu "Node"
    def __init__(self,initdata): # sukuriama pagrindinė finkcija su kitamuoju self ir "initdada" kuris turi būti paduotas kuriant objektą
        self.data = initdata # prie objekto self sukuriamas "data" strukūra su "initdata" duomenimis 
        self.next = None # prie objekto self sukuriamas tuščias objektas "next" su reikšme None 

    def getData(self): # funkcija imanti objektą "self"
        return self.data # grąžina šio objekto vertę

    def getNext(self): # funkcija imanti objektą "self" 
        return self.next # grąžina kintamojo "next" reikšmę (grąžina sekantį objektą)

    def setData(self,newdata): # funkcija imanti objektą "self" ir "newdata", kuris gaunamas kviečiant funkciją 
        self.data = newdata # nauja vertė priskiriami objektui "data" kintamajam 

    def setNext(self,newnext): # funkcija imanti objektą "self" ir "newnext", kuris gaunamas kviečiant funkciją 
        self.next = newnext #  priskiriamas naujas objektas "next" kintamajam 

#temp = Node(93)
#print (temp.getData())

class OrderedList: # klasė pavadinimu "OrderedList"

    def __init__(self): # sukuriama pagrindinė funkcija su kitamuoju self
        self.head = None # prie objekto self sukuriamas tuščias masyvas "head" su reikšme None 
    
    def isEmpty(self): # funkcija "isEmpty" imanti objektą "self"
        return self.head == None # grąžina loginį "True" jei kintamojo "head" reikšmė lygi "None"
    
    def size(self): # funkcija "size" imanti objektą "self"
        current = self.head # kintamajam "current" priskiriami duomenys iš klasės "self" "head" kintamojo
        count = 0 # skaitiklis priskiriamas nuliu
        while current != None: # ciklas vyksta iki "current" pasiekiamas tuščias objektas
            count = count + 1 # prie skaitiklio pridedamas + 1
            current = current.getNext() #grąžinamas sekantis objektas 
        
        return count # grąžinamas esmų verčių kiekis
    
    def remove(self,item): # funkcija "remove" imanti "self" ir "item", kurio vertė nusprendžiama kviečiant funkciją
        current = self.head #priskiriamas self.head objektas "current" kintamajam
        previous = None # priskiriama kintamajam "previous" vertę None
        found = False # kintamajam "found" priskiriama loginis False 
        while not found: # ciklas sukasi iki "found" tampa loginiu "True"
            if current.getData() == item: # lyginama esamo objekto "Data" vertė su įvesta iš funkcijos
                found = True # jei randama vertė found pakeičia loginią reikšmę į "True"
            else: # kitu atveju
                previous = current #išsaugomas buvęs objektas į kintamajį "previous"
                current = current.getNext() # imamas sekantis objektas

        if previous == None: # sąlyga vykdoma jei "previuos" objektas yra tuščias 
            self.head = current.getNext() #
        else: # kitu atveju
            previous.setNext(current.getNext()) #pašalinamas objektas užrašant puvusios objekto nuorodą į jį su sekančia objekto nuoroda
    
    def search(self,item): # funkcija "search" imanti "self" ir "item", kurio vertė nusprendžiama kviečiant funkciją
        current = self.head # priskiriamas self.head objektas "current" kintamajam
        found = False # kintamajam "found" priskiriama loginis False 
        stop = False # kintamajam "stop" priskiriama loginis False 
        while current != None and not found and not stop: # ciklas sukasi iki reikmė "current" nėra tuščias ir "found" loginė reikšmė nėra "True" ir "stop" loginė reikšmė nėra "True"
            if current.getData() == item: # sąlyga tenkinama kai nauja vertė yra mažesnė už "item" vertę
                found = True # pakeičiam "found" vertę į lokinį True
            else: # jei netenkina salygos
                if current.getData() > item: # sąlyga tenkinama jei ieškomo elemto vertė yra mažesnė už šiuo metu esančio objekto kintamojo data vertę
                    stop = True # gražinamas loginis "True"
                else: # jei netenkina salygos
                    current = current.getNext() # imamas sekantis objektas

        return found # gražina loginę reikšmę "True" jei rado reikšmę, jei ne - "False"
    
    def add(self,item): # funkcija "add" imanti klasę "self" ir "item", kuris turi būti duodamas kviečiant šią funkciją
        current = self.head # kintamjam "current" priskiriami duomenys iš klasės "self" "head" kintamojo
        previous = None # kintamajam priskiriama reikšmė "None"
        stop = False # kintamajam priskiriama loginė reikšmė "False"
        while current != None and not stop:# ciklas sukasi iki reikmė "current" nėra tuščias ir "stop" loginė reikšmė nėra "True"
            if current.getData() > item: # sąlyga tenkinama kai nauja vertė yra mažesnė už "item" vertę
                stop = True # pakeičiam "stop" vertę į lokinį True
            else: # jei netenkina salygos
                previous = current # priskiriame "current" objektą "previous" kintamajam 
                current = current.getNext() # kintamajm current priskiriame "Node" klasės funkcijos kintamojo reikšmę
                
                

        temp = Node(item) # sukuriama objektas "temp" pagal klasę Node()
        if previous == None: # sąlyga tenkinama kai kintamasi "previous" 
            temp.setNext(self.head) # funkcijai siunčiama senoji vertė (vertė kuri buvo anksčiau įrašyta)
            self.head = temp # priskiriama naujas objektas "head" kintamajam
        else: # jei netenkinama sąlyga
            temp.setNext(current) # nustatomas sekantis kintamasi "temp" objektui 
            previous.setNext(temp) # nustatomas sekantis kintamaisi "previos" objektui

mylist = OrderedList() # sukuriamas objektas pagal klasę "OrderLisr"
mylist.add(31) # pridedama į sąrašą vertė - 31
mylist.add(77) # pridedama į sąrašą vertė - 77
mylist.add(17) # pridedama į sąrašą vertė - 17
mylist.add(93) # pridedama į sąrašą vertė - 93
mylist.add(26) # pridedama į sąrašą vertė - 26
mylist.add(54) # pridedama į sąrašą vertė - 54

#print(mylist.size()) # į terminalą išvedama sąraše esančių duomenų kiekį
#print(mylist.search(93)) # išveda ar kintamasi "93" yra sąraše
#print(mylist.search(100)) # išveda ar kintamasi "100" yra sąraše