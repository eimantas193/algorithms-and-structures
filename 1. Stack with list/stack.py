from basic import Stack # importuojamas Stack klasė iš basic.py failo
#from pythonds.basic import Stack
s=Stack() # s kintamajam priskiriama Stack klasė per kurį bus pasiekiamos jos funkcijos 

print(s.isEmpty()) # kreipiasi į funkciją isEmpyty() ir išveda rezultatą į ekraną
s.push(4) # kreipiasi į funkciją push() ir siunčia int tipo skaičių 4
s.push('dog') # kreipiasi į funkciją push() ir siunčia eilutę 'dog'
print(s.peek()) # kreipiasi į funkciją peek() ir išveda rezultatą į ekraną
s.push(True) # kreipiasi į funkciją push() ir siunčia būseną True
print(s.size()) # kreipiasi į funkciją size() ir išveda rezultatą į ekraną
print(s.isEmpty()) # kreipiasi į funkciją isEmpyty() ir išveda rezultatą į ekraną
s.push(8.4) # kreipiasi į funkciją push() ir siunčia flow tipo skaičių 8.4
print(s.pop())  # kreipiasi į funkciją pop() ir išveda gautą rezultatą į ekraną
print(s.pop())  # kreipiasi į funkciją pop() ir išveda gautą rezultatą į ekraną
print(s.size())  # kreipiasi į funkciją size() ir išveda rezultatą į ekraną


