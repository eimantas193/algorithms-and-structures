#turtle
import turtle # importuojamas vėžlys

myTurtle = turtle.Turtle()  # kitamajam priskiriamas vėžlio klasė
myWin = turtle.Screen()  # kitamajam priskiriama vėžlio darbo lauko klasė

def drawSpiral(myTurtle, lineLen): # spiralės piešimo klasė
    if lineLen > 0: # sąlyga vykdoma jei spiralės ilgis daugiau nei nulis
        myTurtle.forward(lineLen) # judama tiesiai per numatytą vienetų skaičių
        myTurtle.right(90) # pasukama 90 laipsnių kampu į dešinę
        drawSpiral(myTurtle,lineLen-5) # vykdome rekursiją kviesdami šia funkciją iš naujo sumažindami linijos ilgį 5 vienetais

drawSpiral(myTurtle,100)  # į funkciją siunčiama vėžlio klasės kintamasi ir linijos ilgis
myWin.exitonclick() # uždaromas ekranas kai paspaudžiama darbo laukas su pele
