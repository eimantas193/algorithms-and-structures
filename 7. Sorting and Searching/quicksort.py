def quickSort(alist): # greitojo rūšiavimo algoritmo įgyvendinimo funkcija (ima nerušiuotą sąrašą)
   quickSortHelper(alist,0,len(alist)-1) # siunčiami duomenys į atskirą rekuriantinią funkciją ( sąrašas, pirmo nario indeksas, paskutinio nario indeksas)

def quickSortHelper(alist,first,last): # rekuriantinė greitojo rūšiavimo funkcija 
   if first<last: # sąlyga vykdoma jei pirmojo nario indeksas mažensis už paskutiniojo nario

       splitpoint = partition(alist,first,last) # sąrašo dalinimo tašką

       quickSortHelper(alist,first,splitpoint-1) # kviečiama ta pati funkcija tik rušiuojama nuo pirmo kintamojo iki padalinimo taško 
       quickSortHelper(alist,splitpoint+1,last) # kviečiama ta pati funkcija tik rušiuojama nuo padalinimo taško iki pabaigos


def partition(alist,first,last): # greitojo rūšiavimo algoritmo skaldymo funkcija
   pivotvalue = alist[first] # paiimamas kintamasis su indeksu "first"

   leftmark = first+1 # paimamas sekantis indeksas nuo pormojo
   rightmark = last # paiimamas paskutinis sąrašo indeksas

   done = False # kintamasis indikuojantis, kad baigtas 
   while not done: # vykdoma iki done nebus loginis "True"

       while leftmark <= rightmark and alist[leftmark] <= pivotvalue: # ciklas vykdomas iki "leftmark" yra mažesnis arba lygus už "rightmark" ir "alist" sąrašo kintamasis esantis
           # indeksu "leftmark" mažesnis už "pivotvalue" reikšmę
           leftmark = leftmark + 1 # prie indekso "leftmark" pridedamas vienetas

       while alist[rightmark] >= pivotvalue and rightmark >= leftmark: # ciklas vykdomas iki "alist" kintamojo esančio "rightmark" indeksas mažesnis už "pivotvalue" ir "rightmark" indeksas didesnis
           # arba lygus indeksu "leftmark"
           rightmark = rightmark -1 # iš indekso "leftmark" atimamas vienetas

       if rightmark < leftmark: # jei "rightmark" indekasas maženis už "leftmark"
           done = True # baigiamas ciklas
       else: # kitu atveju
           temp = alist[leftmark] # laikinajam kintamajam priskiriama vertė "alist" kintamajo esančio "leftmark" indeksu
           alist[leftmark] = alist[rightmark] # sąrašo "alist" kintamojo esančio "leftmark" indeksu vertė užrašoma to pačio sąrašo kintamojo verte esančia indeksu "rightmark"
           alist[rightmark] = temp # sąrašo "alist" kintamojo esančio "leftmark" indeksu vertė užrašoma anksčiau išsaugota verte esančia "temp" kintamajame

   temp = alist[first] # laikinajam kintamajam priskiriama vertė "alist" kintamajo esančio "first" indeksu 
   alist[first] = alist[rightmark] # sąrašo "alist" kintamojo esančio "first" indeksu vertė užrašoma to pačio sąrašo kintamojo verte esančia indeksu "rightmark"
   alist[rightmark] = temp # sąrašo "alist" kintamojo esančio "rightmark" indeksu vertė užrašoma anksčiau išsaugota verte esančia "temp" kintamajame


   return rightmark # gražinamas kintamajo indeksas "rightmark"

alist = [54,26,93,17,77,31,44,55,20] # duomenų sąrašas
quickSort(alist) # siunčiamas sąrašas į rušiavimo algoritmą 
print(alist) # išspausdinamas surušiuotas sąrašas į ekraną
