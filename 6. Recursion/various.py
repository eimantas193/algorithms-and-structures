#Without reccursion
def listsum1(numList): # kintamųjų sudėjimo funkcija
    theSum = 0 # sumos kintamais
    for i in numList: # einame per visą sąrašą
        theSum = theSum + i # kievieną kinatamajį pridedam prie sumos kintamojo 
    return theSum # grąžinam sumą

 # print(listsum1([1,3,5,7,9])) # išspausdianme funkcijos rezultatą į kurią siunčiam sąrašą kintamųjų kuriuos sudedame

#With Recursion
def listsum2(numList): # kintamųjų sudėjimo funkcija naudojant rekursiją
   if len(numList) == 1: # jei sąrašo ilgis lygus vienetui
        return numList[0] # grąžinamas šio sąrašo pirmasis narys
   else: # kitu atveju
        return numList[0] + listsum2(numList[1:]) # naudojant rekursiją prie esamo nulinio elemnto pridedama suma iš buvusių rekursijų

# print(listsum2([1,3,5,7,9])) # išspausdianme funkcijos rezultatą į kurią siunčiam sąrašą kintamųjų kuriuos sudedame

#Numbers to String
def toStr(n,base): # konvertavimo funkcija
   convertString = "0123456789ABCDEF" # konvertavimo eilutė pagal kurią bus ieškoma atitikmenų 
   if n < base: # jei skaičius yra mažesnis už pagrindą ( mūsų atveju šešioliktainis)
      return convertString[n] # tada grąžinama reikšmė pagal indeksą iš "convertString"
   else: # kitu atveju
      return toStr(n//base,base) + convertString[n%base] # daliname skaičių iš pagrindo ir jį siunčiame su pagrindu vėl į šią funkciją (rekursiją) 

#print(toStr(1453,16))


#Numbers to String with stack
from pythonds.basic import Stack  # importuojame dėklą

rStack = Stack() #priskiriame kitamajam dėklo klasę

def toStr(n,base): # konvertavimo funkcija
    convertString = "0123456789ABCDEF" # konvertavimo eilutė pagal kurią bus ieškoma atitikmenų 
    while n > 0: # ciklas sukamas iki kintamasis lygus nuliu
        if n < base: # jei kintamais mažesnis nei pagrindas
            rStack.push(convertString[n]) # jo konvertuota versiją saugom dėkle
        else: #  kitu atveju 
            rStack.push(convertString[n % base]) # daliname skaičių iš pagrindo ir ieškome konvertavimo eilutėje jo indekso pagal liekaną
        n = n // base # daliname skaičių iš pagrindo
    res = "" #  sukuriam tuščią rezultatų eilutę
    while not rStack.isEmpty(): # ciklas vykdomas iki dėklas tampa tuščiu
        res = res + str(rStack.pop()) # iš dėklo pabaigos imamas kintamasis po vieną ir sujungiamas į eilutės tipo kintamajį
    return res # grąžinamas rezultatas

print(toStr(1453,16)) # išspausdianme funkcijos rezultatą į kurią siunčiam sąrašą kintamųjų kuriuos sudedame
