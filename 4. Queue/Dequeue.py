class Deque: # klasė pavadinimu "Deque"
	    def __init__(self): # funkcija __init__ pasileidžianti automatiškai kai priskiriama klasė kintamajam  ir su įvedimu "self" kuris rodo į šį kintamajį
	        self.items = [] # sukuriamas sąrašas pavadinimu "items" tam kintamajam
	
	    def isEmpty(self): # funkcija skirta patikrinti ar sąrašas turi kintamųjų ir ne (kimamas tik pats kintamasis)
	        return self.items == [] # grąžinama reikšmė "True" jei sąrašas tuščias arba "False" jei yra bent vienas kintamasis
	
	    def addFront(self, item): # funkcija imanti klasę ir naują reikšmę 
	        self.items.append(item) # prie esamos sąrašo pabaigos pridedama nauja reikėmę
	
	    def addRear(self, item): # funkcija imanti klasę ir naują reikšmę 
	        self.items.insert(0,item) # prie esamos sąrašo pradžios pridedama nauja reikėmę
	
	    def removeFront(self): # funkcija imanti tik klasę
	        return self.items.pop() # iš sąrašo pabaigos pašalinamas kintamasis ir grąžinamas atgal į kvietusę funkciją
	
	    def removeRear(self): # funkcija imanti tik klasę
	        return self.items.pop(0) # iš sąrašo pradžios pašalinamas kintamasis ir grąžinamas atgal į kvietusę funkciją
	
	    def size(self): # funkcija imanti tik klasę
	        return len(self.items) # gražinama int tipo kintamasis rodantis kiek elementų likę sąraše
	
d=Deque() # kintamajam d priskiriamas klasė "Deque"
print(d.isEmpty()) # išvedama į konsolę ar klasėje esantis sarašas yra tuščias lognis true jei tučias loginis false jei ne
d.addRear(4) # pridedmas int kintamasis prie klasės sąrašo pabaigos
d.addRear('dog') # pridedmas string kintamasis prie klasės sąrašo pabaigos
d.addFront('cat') # pridedmas string kintamasis prie klasės sąrašo pradžios
d.addFront(True) # pridedmas boolean kintamasis prie klasės sąrašo pradžios
print(d.size()) # išveda į konsolę klasės sąrašo ilgį (arba kiek kintamųjų ten yra)  
print(d.isEmpty()) # išvedama į konsolę ar klasėje esantis sarašas yra tuščias lognis true jei tučias loginis false jei ne
d.addRear(8.4) # pridedmas float kintamasis prie klasės sąrašo pabaigos
print(d.removeRear()) # išvedamas į konsolę ir pašalinmas kintamasis esantis klasės sąrašo pabaigoje
print(d.removeFront()) # išvedamas į konsolę ir pašalinmas kintamasis esantis klasės sąrašo pradžioje
