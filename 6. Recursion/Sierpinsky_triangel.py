import turtle # įportuojamas vėžlys

def drawTriangle(points,color,myTurtle): # trikampio piešimo funkcija
    myTurtle.fillcolor(color) #pasirnakma užpildo spalva
    myTurtle.up() # nustojama piešti
    myTurtle.goto(points[0][0],points[0][1]) # judama į trikampio kairį kampą 
    myTurtle.down() # pradedama piešt
    myTurtle.begin_fill() #kviečiama prieš piešiant figūrą
    myTurtle.goto(points[1][0],points[1][1]) # judama į trikampio viršūnę
    myTurtle.goto(points[2][0],points[2][1]) # judama į trikampio kampą
    myTurtle.goto(points[0][0],points[0][1]) # grįžtama į trikampio pirminį kampą
    myTurtle.end_fill() # kviečiama baigiant piešt figūrą

def getMid(p1,p2): #
    return ( (p1[0]+p2[0]) / 2, (p1[1] + p2[1]) / 2) #

def sierpinski(points,degree,myTurtle): # rekurentinė funkcija skirta "sierpinski" piešimui
    colormap = ['blue','red','green','white','yellow',
                'violet','orange'] # spalvų žemėlapis
    drawTriangle(points,colormap[degree],myTurtle) # kviečiama funkcija skirta piešti trikampiu duodant jo koordinates bei spalvą
    if degree > 0: # jei laipsnis daugiau 
        sierpinski([points[0],
                        getMid(points[0], points[1]),
                        getMid(points[0], points[2])],
                   degree-1, myTurtle) # piešiamas trikampis kairėje 
        sierpinski([points[1],
                        getMid(points[0], points[1]),
                        getMid(points[1], points[2])],
                   degree-1, myTurtle) #  piešiamas trikampis dešinėje
        sierpinski([points[2],
                        getMid(points[2], points[1]),
                        getMid(points[0], points[2])],
                   degree-1, myTurtle) #piešiamas trikampis viršuje

def main(): # pagrindinė programa
   myTurtle = turtle.Turtle() # kintamajam priskiriama vėžlio valdymas
   myWin = turtle.Screen() # kintamajam priskiriama dirbamos aplinkos valdymas
   myPoints = [[-100,-50],[0,100],[100,-50]] # kitamajma priskiriama trikampio viršūnių koordinatės
   sierpinski(myPoints,6,myTurtle) # nubraižomas sierpinski trikampis pagal duotas koordinates
   myWin.exitonclick() #išjungiamas ekranas kai paspaudžiamas pelės klavišas

main() # paleidžiama pagrindinė prohrama
