import turtle # importuojamas vėžlys
# sukurriami orentyrai
PART_OF_PATH = 'O' # galimas kelias
TRIED = '.' # bandytas kelio taškas
OBSTACLE = '+' # kliūtis
DEAD_END = '-' # aklavietė

class Maze: #
    def __init__(self,mazeFileName): # sukuriama funkcija apibūdinati šios klasės kintamajį 
        rowsInMaze = 0 # eilučių kiekis labirinte
        columnsInMaze = 0 # stulpelių kiekis labirinte
        self.mazelist = [] # sukuriamas tuščias klasės sąrašas
        mazeFile = open(mazeFileName,'r') # atidaromas failas ir priskiriamas kintamajam "maze file"
        for line in mazeFile: # ciklas einantis per visas eilutes esančias faile
            rowList = [] # tuščias eilučių sąrašas 
            col = 0 # stulpelių kiekis
            for ch in line[:-1]: # einama per kievieną kintamajį eilutėje
                rowList.append(ch) # pridedamas esamas kintamasis prie eilučių sąrašo pabaigos 
                if ch == 'S': # jei tas kintamasis S
                    self.startRow = rowsInMaze # išsaugoma pradžios eilutė 
                    self.startCol = col # ir išsaugoma pradžios stulpelis 
                col = col + 1 # pridedama vientas prie stulpelių sumos  
            rowsInMaze = rowsInMaze + 1 # pridedama vienetas prie eilučių sumos
            self.mazelist.append(rowList) # prie mazelist sąrašo pabaigos kaip kinatmais pridedamas row list sąrašas
            columnsInMaze = len(rowList) # gaunamas stulpelių skaičius panaudojus len() funkciją kuri grąžina kintamųjų skaičių iš sąrašo

        self.rowsInMaze = rowsInMaze # sukuraimas klasės kintamasis kuriam priskiraima eilučių skaičius labirinte 
        self.columnsInMaze = columnsInMaze # sukuraimas klasės kintamasis kuriam priskiraima stulpelių skaičius labirinte
        self.xTranslate = -columnsInMaze/2 #sukuraimas klasės kintamasis kuriam priskiraima sveikoji x ašis
        self.yTranslate = rowsInMaze/2 #sukuraimas klasės kintamasis kuriam priskiraima sveikoji y ašis
        self.t = turtle.Turtle() # sukuraimas klasės kintamasis kuriam priskiraima vėžlio funkcija
        self.t.shape('turtle') # vėžlio kintamajam pritaikoma vėžlio išvaizda
        self.wn = turtle.Screen() # vėžlys išvedamas į ekraną
        self.wn.setworldcoordinates(-(columnsInMaze-1)/2-.5,-(rowsInMaze-1)/2-.5,(columnsInMaze-1)/2+.5,(rowsInMaze-1)/2+.5) #sukuriamaos ribinės koordinatės vėžliu pagal kurias judės

    def drawMaze(self): # funkcija skirta piešti labirintui 
        self.t.speed(10) # vėžlio judėjimo greitis
        self.wn.tracer(0) # skirta vėžlio animacijoms
        for y in range(self.rowsInMaze): # cinklas einantis per eilutes
            for x in range(self.columnsInMaze): # ciklas einantis per stulpelius
                if self.mazelist[y][x] == OBSTACLE: # jei randama vertė '+'
                    self.drawCenteredBox(x+self.xTranslate,-y+self.yTranslate,'orange') #ekrane atvaizduojama kliūtis pasianudojant funkcija "drawCenteredBox"
        self.t.color('black') # vėžlio splava - juoda
        self.t.fillcolor('blue') # vėžlio užpildo spalva - mėlyna 
        self.wn.update() # atnaujina vėžlio ekraną
        self.wn.tracer(1) # skirta vėžlio animacijoms

    def drawCenteredBox(self,x,y,color): # funkcija skirta piešti kliūtims
        self.t.up() # nustojama piešti
        self.t.goto(x-.5,y-.5) # einama į numatytas kliūties koordinates
        self.t.color(color) # pasirenkama piešimo spalva
        self.t.fillcolor(color) # pasirankma užpildo spalva
        self.t.setheading(90) # pasukama piešimo kryptis 90 lapsnių kampu
        self.t.down() #pasiruoušiama piešti
        self.t.begin_fill() #kviečiama prieš piršiant figūrą
        for i in range(4): # ciklas vykstantis nuo 1 iki 4
            self.t.forward(1) # piešiama linija per vieną matą
            self.t.right(90) # sukama galvutė 90 laipsnių kampu
        self.t.end_fill() #kiviečiama kai baigiama pieršti figūra

    def moveTurtle(self,x,y): #funkcija skirta vėžlio judėjimui imama x y judėjimo koordinatė
        self.t.up() #nustojama piešti
        self.t.setheading(self.t.towards(x+self.xTranslate,-y+self.yTranslate)) # atsukama galvutė į numatytas koordiantes
        self.t.goto(x+self.xTranslate,-y+self.yTranslate) # nujudama link tų koordinačių

    def dropBreadcrumb(self,color): # taško padėjimo funkcija
        self.t.dot(10,color) # padedamas taškas 10 vnt dydžio, bei numatytos spalvos

    def updatePosition(self,row,col,val=None): # vietos atnaujinimo funkcija imanti norima eilutę ir stulpelį 
        if val: # jei nepaduodama reikšmė
            self.mazelist[row][col] = val # priskiriama koordiantei None reikšmė
        self.moveTurtle(col,row) #judama į numatytą koordinatę

        if val == PART_OF_PATH: # jei sprendimo kelias
            color = 'green' # duodama žalia
        elif val == OBSTACLE: # jei kliūtis
            color = 'red' # raudona
        elif val == TRIED: # jei jau bandytas
            color = 'black' # juoda
        elif val == DEAD_END: # jei akligatvis
            color = 'red' # raudona
        else: # kitu atveju
            color = None # nieko 

        if color: # jei yra spalva
            self.dropBreadcrumb(color) # naudojama funkcija padėti taškui tos spalvos

    def isExit(self,row,col): # funkcija tikrinati ar išėjimas ( imanti eilutę ir stulpelį)
        return (row == 0 or 
                row == self.rowsInMaze-1 or
                col == 0 or
                col == self.columnsInMaze-1 ) # turi būti arba nuolineme stulpelyje arba stulpelių skaičius -1 arba nulinėje eilutėje arba eilučių skaičius -1

    def __getitem__(self,idx): #
        return self.mazelist[idx] #


def searchFrom(maze, startRow, startColumn): # kelio ieškojimo funkcija
    # try each of four directions from this point until we find a way out.
    # base Case return values:
    #  1. We have run into an obstacle, return false
    maze.updatePosition(startRow, startColumn) # nurodomos starto koordinatės
    if maze[startRow][startColumn] == OBSTACLE : # tikrinama ar neįėjome į kliūtį
        return False # grąžinama "false" jei taip
    #  2. We have found a square that has already been explored
    if maze[startRow][startColumn] == TRIED or maze[startRow][startColumn] == DEAD_END: # jei  koordinatės yra priskiriamos bandytam keliu arba kalvietei
        return False # gražianma False
    # 3. We have found an outside edge not occupied by an obstacle
    if maze.isExit(startRow,startColumn): # jei radom išėjimą
        maze.updatePosition(startRow, startColumn, PART_OF_PATH) # priskiriame kordiantes prie sprendimo
        return True # ir gražinam True reiškmę
    maze.updatePosition(startRow, startColumn, TRIED) #
    # Otherwise, use logical short circuiting to try each direction
    # in turn (if needed)
    found = searchFrom(maze, startRow-1, startColumn) or \
            searchFrom(maze, startRow+1, startColumn) or \
            searchFrom(maze, startRow, startColumn-1) or \
            searchFrom(maze, startRow, startColumn+1) # tikriname kievieną galimą ėjimą ir einame pirmu galimu keliu
    if found: # jei galimas kelias  
        maze.updatePosition(startRow, startColumn, PART_OF_PATH) # einame juo
    else: # jei negalimas
        maze.updatePosition(startRow, startColumn, DEAD_END) # jei nėra kito kelio priskiriama aklavietei 
    return found # gražiana kelią 


myMaze = Maze('maze.txt') # į Maze suinčiasmas txt failas su labirinto kontūrais
myMaze.drawMaze() # nupiešaime labirintą
myMaze.updatePosition(myMaze.startRow,myMaze.startCol) # pastatome vėžlį į pradines koordiantes

searchFrom(myMaze, myMaze.startRow, myMaze.startCol) # atlieakme paeišką 
