from pythonds.basic import Stack # iš pythonds bibliotekos, basic skripto įkeliama klasė Stack

def postfixEval(postfixExpr): # funkcija su vienu kintamuoju į kurią paduodama eilutė
    operandStack = Stack() # operandStack priskiriama Stack klasė su jos funkcijom
    tokenList = postfixExpr.split() # išskaidomos infixexpr į sąrašą su kiekveinam ženklui priskirtu indeksu

    for token in tokenList: # einama per visus tokenList elementus
        if token in "0123456789": # tikrinama ar esamas elementas yra skaičius
            operandStack.push(int(token)) # jei atitinka sąlygą viršuje pridedamas token elementas prie postfixList sąrašo pabaigoje 
        else: # jei if sąlyga netinka vygdomos apačioje esamos funkcijos
            operand2 = operandStack.pop()  # iš operandStack išimamas paskutinis elementas, kuris priskiriamas operand2
            operand1 = operandStack.pop()  # iš operandStack išimamas paskutinis elementas, kuris priskiriamas operand1
            result = doMath(token,operand1,operand2) # token, operand1 ir operand2 paduodamas į doMath() funkciją ir rezultatas grąžinamas į kintamajį result
            operandStack.push(result) #result kinatmasis yra pridedamas iš galo prie operandStack sąrašo
    return operandStack.pop() #gražinama paskutinė reikšmė iš operandStack sąrašo

def doMath(op, op1, op2): # funkcija su trimis kintamaisiai op-ženklas op1 ir op2 skaičius
    if op == "*":  # jei op yra lygus "*"
        return op1 * op2  # gražinama op1 ir op2 sandauga
    elif op == "/":  # jei op yra lygus  "/"
        return op1 / op2 # gražinama op1 ir op2 dalyba
    elif op == "+": # jei op yra lygus  "+"
        return op1 + op2 # gražinama op1 ir op2 suma
    else: # jei nei viena sąlyga netinka vygdomos apačioje esamos funkcijos
        return op1 - op2  # gražinama op1 ir op2 skirtumas

print(postfixEval('7 8 + 3 2 + /')) # siunčiama eilutė į postfixEval funkciją ir rezultatą išvedama į komados eilutę

#     7    8   +    3    2    +    /
#                        2   
#          8        3    3    5   
#     7    7   15   15   15   15   3