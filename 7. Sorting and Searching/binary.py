#divide and conquer strategy
# pastebėjimas abu kodai veikia tik tuo atveju jei duomenys sąraše yra surušiuoti
def binarySearch(alist, item): #funkcija įgyvendinanti skaldyk ir valdyk metodą (imamas sąrašas ir kintamasis kurį turime rast tame sąraše)
    first = 0 # pirmo nario indeksas
    last = len(alist)-1 # randamas sąrašo paskutinis nario indeksas
    found = False # kintamasis kuris pakeičia iš loginio "False" į loginį "True" jei randamas ieškomas kintamais

    while first<=last and not found: # ciklas vyksta iki "first" kintamasis tampa daugiau arba lygus už "last" arba randmas ieškomas kintamasis
        midpoint = (first + last)//2 # randamas šiuo metu tikrinamo sąrašo vidurys
        if alist[midpoint] == item: # tikrinama ar su indeksu "midpoint" kintamasis yra mūsų ieškomasis
            found = True # gražianm loginį "True" jei taip
        else: # kitu atveju
            if item < alist[midpoint]: # tikrinama ar ieškomas kintamasis yra mažesnis už dabar tikrinamą 
                last = midpoint-1 # jei taip paskutiniojo nario kintamajam "last" priskiriame vidurio vertę - 1 ( -1, nes centrinė vertė jau tikrinta )
            else: # kitu atveju 
                first = midpoint+1 # jei ieškomo kintomojo vertė didesnė už dabartinę vidurio vertę tai pirmo nario "first" vertei priskiriam vidurio vertę + 1 ( +1, nes centrinė vertė jau tikrinta )
	
    return found # grąžinama loginis "True" jei vertė rasta kitu atveju loginis "False"
	
testlist = [0, 1, 2, 8, 13, 17, 19, 32, 42,] #duomenų sąrašas
print(binarySearch(testlist, 3)) # duomenų sąraše ieškomas kintamasis 3 ir išvedame į ekraną loginis "True" jei toks kintamasis ten yra kitu loginis "False"
print(binarySearch(testlist, 13)) # duomenų sąraše ieškomas kintamasis 13 ir išvedame į ekraną loginis "True" jei toks kintamasis ten yra kitu loginis "False"

#recursive search
# pastaba funkcija veikia tik surušiuotais sąrašais
def binarySearch(alist, item): #funkcija įgyvendinanti rekusinės paieškos metodą (imamas sąrašas ir kintamasis kurį turime rast tame sąraše)
    if len(alist) == 0:  # jei ieškomajo sąrašo ilgis lygus 0
        return False # gražinamas loginis "False"
    else: # kitu atveju
        midpoint = len(alist)//2 # ieškoma dabartinio sąrašo vidurys
        if alist[midpoint]==item: # jei sąrašo vidurio narys lygus ieškomam skaičiu 
          return True # grąžinamas loginis true
        else: # kitu atveju
          if item<alist[midpoint]: # jei ieškomas skaičius yra mažesnis už vidurio sąrašo vertę  
            return binarySearch(alist[:midpoint],item) # į sekančia rekursiją sunčiamas sąrašas su visom vertėm iki vidurio taško (":midpoint" reiškia visos vertės iki tam tikros nurodytos)
          else: # kitu atveju
            return binarySearch(alist[midpoint+1:],item) # į sekančia rekursiją sunčiamas sąrašas su visom vertėm nuo vidurio taško iki sąrašo pabaigos
	
testlist = [0, 1, 2, 8, 13, 17, 19, 32, 42,] # duomenų sąrašas
print(binarySearch(testlist, 3)) # duomenų sąraše ieškomas kintamasis 3 ir išvedame į ekraną loginis "True" jei toks kintamasis ten yra kitu loginis "False"
print(binarySearch(testlist, 13)) # duomenų sąraše ieškomas kintamasis 13 ir išvedame į ekraną loginis "True" jei toks kintamasis ten yra kitu loginis "False"