from multiprocessing import Process # importuojama "Process" klasė iš "multiprocessing" paketo
import os # importuojama operacinės sistemos klasė

def info(title): # funkcija info (priima string kintamajį)
    print(title) # išveda "title" turinį į ekraną
    print('module name:', __name__) # išvedafunkcijos __name__ pagrindinio proceso pavadinimas
    print('parent process:', os.getppid()) # grąžinamas tėviškasis proceso id
    print('process id:', os.getpid()) # proceso id

def f(name): # funkcija f
    info('function f') # kieviečia kitą funkciją ir peduoda string eilutę
    print('hello', name) # išveda į ekraną eilutė su "hello" ir kitamajame esančia informacija

if __name__ == '__main__': # pagrindins procesas
    info('main line') # kviečiama informacinė funkcija
    p = Process(target=f, args=('bob',)) # sukuriamas naujas procesas vykdantis funkciją f ir siunčiantis "string" eilutę ir priskiriamas kintamajam "p"
    p.start() # pradedamas procesas p
    p.join() # laukiama iki baigsis procesas
