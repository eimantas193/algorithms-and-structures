import logging #importuojama laiko monitoringas
import threading #importuojama gijų klasė
import time # imprtuojamas laiko klasė

def thread_function(name):
    logging.info("Thread %s: starting", name) # išvedamas tekstas į ekraną
    time.sleep(2) # gija miega 2 sekundes
    logging.info("Thread %s: finishing", name) # išvedamas tekstas į ekraną

if __name__ == "__main__": # pagrindinis procesas
    format = "%(asctime)s: %(message)s" # apibrėžiamas išvedimo formatas
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S") # sokonfiguojama monitoringo klasė

    threads = list() # sukuriamas tuščias gijų sąrašas
    for index in range(3): # ciklas vyksta iteracijas
        logging.info("Main    : create and start thread %d.", index) # išvedama informacija kuri gija bus paleista
        x = threading.Thread(target=thread_function, args=(index,)) # inicelizuojama gija
        threads.append(x) # gija pridedamas prie sąrašo
        x.start() # pradedama gija

    for index, thread in enumerate(threads): # einama per gijas
        logging.info("Main    : before joining thread %d.", index) # išvedama informacija į ekraną
        thread.join() # laukiama iki gija baigs darbą
        logging.info("Main    : thread %d done", index) # išvedama informacija į ekraną
