def sequentialSearch(alist, item): # nuosekliosios paieškos įgyvendinimo funkcija (imamas sąrašas ir kintamasis kurį turime rast tame sąraše)
	pos = 0 # pirmai pozicijai priskiriamas 0
	found = False # kintamajam "found" priskiriamas loginis False kuris indikuoja jei ieškomas skaičius yra rastas
	
	while pos < len(alist) and not found: # ciklas vyksta iki dabartinė pozicija daugiau už sąrašo ilgį arba rastas ieškomas skaičius
	    if alist[pos] == item: # jei dabartinio indekso pozicijoje esantis kintamasis lygus ieškomam 
	        found = True # kintamajam "found" priskiriamas loginis True
	    else: # jei ne
	        pos = pos+1 # prie dabartinio indekso pridedamas vienetas 
	
	return found # grąžinama loginis "True" jei vertė rasta kitu atveju loginis "False"
	
testlist = [1, 2, 32, 8, 17, 19, 42, 13, 0] # duomenų sąrašas
print(sequentialSearch(testlist, 3)) # duomenų sąraše ieškomas kintamasis 3 ir išvedame į ekraną loginis "True" jei toks kintamasis ten yra kitu loginis "False"
print(sequentialSearch(testlist, 13)) # duomenų sąraše ieškomas kintamasis 13 ir išvedame į ekraną loginis "True" jei toks kintamasis ten yra kitu loginis "False"

# pastaba funkcija veikia tik surušiuotais sąrašais
def orderedSequentialSearch(alist, item): # nuosekliosios paieškos įgyvendinimo funkcija (imamas sąrašas ir kintamasis kurį turime rast tame sąraše)
    pos = 0 # pirmai pozicijai priskiriamas 0
    found = False # kintamajam "found" priskiriamas loginis False kuris indikuoja jei ieškomas skaičius yra rastas
    stop = False # sustabdomas ciklas jei šio kintamojo vertė tampa "True"
    while pos < len(alist) and not found and not stop: # ciklas vyksta iki dabartinė pozicija daugiau už sąrašo ilgį arba rastas ieškomas skaičius arba ieškomoji vertė tampa mažesnė už tikrinamąją
        if alist[pos] == item: # jei dabartinio indekso pozicijoje esantis kintamasis lygus ieškomam 
            found = True # kintamajam "found" priskiriamas loginis True
        else: # jei ne
            if alist[pos] > item: # jei dabartinės pozicijos kintamasis yra didenis už ieškomjo
                stop = True # "stop" kitmajam priskiriame "True" vertę
            else: # jei ne
                pos = pos+1 # prie dabartinio indekso pridedamas vienetas 
	
    return found # grąžinama loginis "True" jei vertė rasta kitu atveju loginis "False"
	
testlist = [0, 1, 2, 8, 13, 17, 19, 32, 42,] # duomenų sąrašas
print(orderedSequentialSearch(testlist, 3)) # duomenų sąraše ieškomas kintamasis 3 ir išvedame į ekraną loginis "True" jei toks kintamasis ten yra kitu loginis "False"
print(orderedSequentialSearch(testlist, 13)) # duomenų sąraše ieškomas kintamasis 13 ir išvedame į ekraną loginis "True" jei toks kintamasis ten yra kitu loginis "False"