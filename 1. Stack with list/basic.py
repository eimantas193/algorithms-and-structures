class Stack: # klasė pavadinimu Stack
     def __init__(self): # inicijuojama pagrindinė funkcija
         self.items = [] # priskiriamas sąrašas self funkcijai 

     def isEmpty(self): # funkcija isEmpty() ima tik vieną argumentą self 
         return self.items == [] # gražinama True jei sąrašas tuščias kitu atveju False

     def push(self, item): # funkcija push() ima argumentą self ir gautą reikšmę iš kreipimosi
         self.items.append(item) # prie sąrašo items prideda iš galo duotąją reikšmę

     def pop(self): # funkcija pop() ima tik vieną argumentą self 
         return self.items.pop() # iš sąrašo pašalina paskutinį narį ir gražina jo reikšmę

     def peek(self): # funkcija peek() ima tik vieną argumentą self 
         return self.items[len(self.items)-1] # iš sąrašo grąžina paskutinę reikšmę

     def size(self): # funkcija size() ima tik vieną argumentą self 
         return len(self.items) # gražina sąrašo dydį