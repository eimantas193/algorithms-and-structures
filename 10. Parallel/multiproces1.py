from multiprocessing import Process, Queue # importuojama iš "multiprocessing" paketo "Process" ir "Queue" klases

def f(q): # funkcija f su komunikacija iš "Queue"
    q.put([42, None, 'hello']) # į pagrindinį procesą gražinamas sąrašas per "Queue" klasę

if __name__ == '__main__':  # pagrindinis procesas
    q = Queue() # kintamjam  q priskiriama Queue komunikacija
    p = Process(target=f, args=(q,)) # inicelizuojamas naujas procesas su komunikacijos kintamuoju q
    p.start() # pradedamas naujas procesas 
    print(q.get()) # gauna sąrašą iš f funkcijos per "Queue" klasę
    p.join() # laukiama iki procesas bus baigtas 