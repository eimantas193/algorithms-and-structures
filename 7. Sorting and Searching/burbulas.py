def bubbleSort(alist): # burbulinis rūšiavimas funkcija (imamas nerušiuotas sąrašas)
    for passnum in range(len(alist)-1,0,-1): # sukamas ciklas kuris sukasi atvirščiai (nuo didžiausios vertės iki mažiausios) kuris priklauso nuo sąrašo ilgio - 1 (papildomas narys)
        for i in range(passnum): # sukamas dar vienas ciklas per mūsų sąrašo parinką ilgį 
            if alist[i]>alist[i+1]: # jei sekantis elemantas yra maženis už dabartinį
                temp = alist[i] # išsaugome dabartinę vertę laikinajame kintamajame 
                alist[i] = alist[i+1] # prilyginam sekančio elemento vertę dabartiniai pozicijai
                alist[i+1] = temp # įrašome laikinai išsaugotą vertę į sekančia poziciją

alist = [54,26,93,17,77,31,44,55,20] # duomenų sąrašas
bubbleSort(alist) # siunčiamas sąrašas į rušiavimo algoritmą 
print(alist) # išspausdinamas surušiuotas sąrašas į ekraną

# kitoks įgyvendinimas vietoj for pirmo ciklo naudojama while
def shortBubbleSort(alist): # trumpasis burbulinis rūšiavimas funkcija (imamas nerušiuotas sąrašas)
    exchanges = True # "exchanges" priskiriame loginį "True"
    passnum = len(alist)-1 # randame sąrašo ilgį
    while passnum > 0 and exchanges: # ciklas vykdomas jei sąrašo ilgis daugiau nei 0 bei "exchanges" yra loginis "True"
       exchanges = False # "exchanges" priskiriame loginį "False"
       for i in range(passnum): # sukamas dar vienas ciklas per mūsų sąrašo parinką ilgį 
           if alist[i]>alist[i+1]: # jei sekantis elemantas yra maženis už dabartinį
               exchanges = True # "exchanges" priskiriame loginį "True"
               temp = alist[i] # išsaugome dabartinę vertę laikinajame kintamajame
               alist[i] = alist[i+1] # prilyginam sekančio elemento vertę dabartiniai pozicijai
               alist[i+1] = temp # įrašome laikinai išsaugotą vertę į sekančia poziciją
       passnum = passnum-1 # mažinam rūšiavimo kelią po vienetą

alist=[20,30,40,90,50,60,70,80,100,110] # duomenų sąrašas
shortBubbleSort(alist) # siunčiamas sąrašas į rušiavimo algoritmą 
print(alist) # išspausdinamas surušiuotas sąrašas į ekraną