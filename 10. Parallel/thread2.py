import threading #importuojama gijų klasė
import time # imprtuojamas laiko klasė

class myThread (threading.Thread):  #gijos klasė
   def __init__(self, threadID, name, counter): # pagrindinė funkcija kuri ima save (self) gijos ID, vardą , laukimo laikas
      threading.Thread.__init__(self) #inicelizuojama gija
      self.threadID = threadID # priskiriamas jai ID
      self.name = name # priskiriamas vardas
      self.counter = counter # priskiriamas laukimas laikas
   def run(self): # funkcija "run" imanti tik self argumentą
      print ("Starting " + self.name) # išvedama informacija į ekraną
      # Get lock to synchronize threads
      threadLock.acquire() # gija įeina į kritinę sekciją (užrakinama ir kita gija priversta laukt)
      print_time(self.name, self.counter, 3) # gija įeina į laiko "print_time" funkciją
      # Free lock to release next thread
      threadLock.release() # gija išeina iš kritinės sekcijos ( atleidžiamas užraktas)

def print_time(threadName, delay, counter): # laiko spausdinimo funkcija
   while counter: # veikia iki skaitliukas pasieks 0
      time.sleep(delay) #gija miega 
      print ("%s: %s" % (threadName, time.ctime(time.time()))) # išspausdinamas laikas
      counter -= 1 # -1 nuo skaitliuko

threadLock = threading.Lock()  #sukuriamas gijų užrakinimas
threads = [] # sukurimas tuščias gijų sąrašas

# Create new threads
thread1 = myThread(1, "Thread-1", 1) # inicelizuojama gija
thread2 = myThread(2, "Thread-2", 2) # inicelizuojama gija

# Start new Threads
thread1.start() # paleidžiama gija
thread2.start() # paleidžiama gija

# Add threads to thread list
threads.append(thread1) # pridedama gija į sąrašą
threads.append(thread2) # pridedama gija į sąrašą

# Wait for all threads to complete
for t in threads: # einama per visas gija
    t.join() # laukiama iki gija baigs darbą
print ("Exiting Main Thread") #išvedama informacija į ekraną