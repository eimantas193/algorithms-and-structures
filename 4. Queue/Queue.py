class Queue:  # klasė pavadinimu "Queue"
	    def __init__(self): # funkcija __init__ pasileidžianti automatiškai kai priskiriama klasė kintamajam  ir su įvedimu "self" kuris rodo į šį kintamajį
	        self.items = [] # sukuriamas sąrašas pavadinimu "items" tam kintamajam
	
	    def isEmpty(self): # funkcija skirta patikrinti ar sąrašas turi kintamųjų ir ne (kimamas tik pats kintamasis)
	        return self.items == [] # grąžinama reikšmė "True" jei sąrašas tuščias arba "False" jei yra bent vienas kintamasis
	
	    def enqueue(self, item): # funkcija imanti klasę ir naują reikšmę 
	        self.items.insert(0,item) # prie esamos sąrašo pradžios pridedama nauja reikėmę
	
	    def dequeue(self): # funkcija imanti tik klasę
	        return self.items.pop() # iš sąrašo pabaigos pašalinamas kintamasis ir grąžinamas atgal į kvietusę funkciją
	
	    def size(self): # funkcija imanti tik klasę
	        return len(self.items) # gražinama int tipo kintamasis rodantis kiek elementų likę sąraše
	
q=Queue() # kintamajam d priskiriamas klasė "Queue"
	
q.enqueue(4) # pridedmas int kintamasis prie klasės sąrašo pradžios
q.enqueue('dog') # pridedmas string kintamasis prie klasės sąrašo pradžios
q.enqueue(True) # pridedmas boolean kintamasis prie klasės sąrašo pradžios
print(q.size()) # išveda į konsolę klasės sąrašo ilgį (arba kiek kintamųjų ten yra)  