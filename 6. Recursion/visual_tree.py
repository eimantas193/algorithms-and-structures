#tree
import turtle # importuojamas vėžlys

def tree(branchLen,t): # 
    if branchLen > 5: # jei šakos ilgis ilgesnis nei 5
        t.forward(branchLen) # piešiamas šaka 
        t.right(20) # pasukama 20 laipsnių į dešinę
        tree(branchLen-15,t) # iš esamos šakos piešiama šaka kairę 20 laipsnių kampu į dešinę nuo centro
        t.left(40) # pasukama 40 laipsnių į kairę
        tree(branchLen-15,t) # iš esamos šakos piešiama šaka kairę 20 laipsnių kampu į dešinę nuo centro
        t.right(20) # pasukama 20 laipsnių į dešinę
        t.backward(branchLen) # grįžtama šaka atgal

def main(): # pagrindinė funkcija
    t = turtle.Turtle() # kintamajma priskiriamas vėžlys
    myWin = turtle.Screen() # kintamajm priskiriamas vėžlio veikimo laukas
    t.left(90) # pasukamas vėžlys 90 laipsnių kampu į kairę
    t.up() # nutraukiamas piešimas
    t.backward(100) # judama 100 žingsnių atgal
    t.down() # pradedama vėl piešti
    t.color("green") # pasirenkamas piešimo spalva
    tree(75,t) # į funkciją piešti šakoms siunčimas šakų ilgis ir vėžlio kintamais
    myWin.exitonclick() # uždaromas ekranas kai paspaudžiama darbo laukas su pele

main() # paleidžiama pagrindinė funkcija