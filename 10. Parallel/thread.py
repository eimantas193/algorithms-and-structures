import logging #importuojama laiko monitoringas

import threading #importuojama gijų klasė

import time # imprtuojamas laiko klasė

def thread_function(name): #

    logging.info("Thread %s: starting", name)  # išvedamas tekstas į ekraną

    time.sleep(2) # gija miega 2 sekundes

    logging.info("Thread %s: finishing", name)  # išvedamas tekstas į ekraną


if __name__ == "__main__": # pagrindinis procesas

    format = "%(asctime)s: %(message)s" # apibrėžiamas išvedimo formatas
    logging.basicConfig(format=format, level=logging.INFO, datefmt="%H:%M:%S") # sokonfiguojama monitoringo klasė
    logging.info("Main    : before creating thread") # išvedamas tekstas į ekraną
    x = threading.Thread(target=thread_function, args=(1,)) # sukurima gija vkdanti funkciją "thread_function" su arguemtu 1
    logging.info("Main    : before running thread") # išvedamas tekstas į ekraną
    x.start() #pradedama giją
    logging.info("Main    : wait for the thread to finish") # išvedamas tekstas į ekraną
    x.join() # laukiama iki gija baigs darbą
    logging.info("Main    : all done")  # išvedamas tekstas į ekraną