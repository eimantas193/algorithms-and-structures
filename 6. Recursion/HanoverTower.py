def moveTower(height,fromPole, toPole, withPole): #rekurentinė funkcija
    if height >= 1: # sąlyga vykdoma jei aukštis yra daugiau arba lygu už vieną
        moveTower(height-1,fromPole,withPole,toPole) # kviečiama tapati funkcija tik keičiami parametrai ( vykdoma rekurencija )
        moveDisk(fromPole,toPole) # kievčiama funkcija išvedimui į ekraną
        moveTower(height-1,withPole,toPole,fromPole) # kviečiama tapati funkcija tik keičiami parametrai ( vykdoma rekurencija )

def moveDisk(fp,tp): # spausdinimo funkcija
    print("moving disk from",fp,"to",tp) # į ekraną išvedamas atsakymas

moveTower(3,"A","B","C") # siunčiami parametrai į funkciją