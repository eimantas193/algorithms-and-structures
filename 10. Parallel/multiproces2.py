from multiprocessing import Pool, TimeoutError # iš "multiprocessing" paketo importuojama "Pool" ir "TimeoutError" klasės
import time # importuokamas laiko klasė
import os # importuojamas operacinė sistemos klasė

def f(x): # funkcija f imanti sakičių
    return x*x # gražinamas sakičiau kvadras

if __name__ == '__main__': # pagrindinis procesas

    with Pool(processes=4) as pool: # pradeda 4 procesus

        print(pool.map(f, range(10))) # siunčia į procesus paraleliai visus skaičius nuo 1 iki 10 ir juos gražina kaip sąrašą

        for i in pool.imap_unordered(f, range(10)): # siunčia į skirtingus procesus po vieną nuo 1 iki 10 
            print(i) # ir juos po vieną išspausdina

        res = pool.apply_async(f, (20,))  # siunčia vieną kintamajį  procesą
        print(res.get(timeout=1))  # rezultatas išvedamas į ekraną

        res = pool.apply_async(os.getpid, ())  # iškviečiamas procesas kuris išveda sukurto proceso id
        print(res.get(timeout=1)) # išvedamas id į ekraną

        multiple_results = [pool.apply_async(os.getpid, ()) for i in range(4)] # kviečiami 4 procesai kurie grąžina savo id
        print([res.get(timeout=1) for res in multiple_results]) # išvedama į ekraną jų id

        res = pool.apply_async(time.sleep, (10,)) # sukuriama procesas kuris miega 10 sekundžių
        try: # bandoma įvykdyti 
            print(res.get(timeout=1)) # rezultato gavimo operaciją
        except TimeoutError: # jei tai "TimeoutError" 
            print("We lacked patience and got a multiprocessing.TimeoutError") # išvedamas tekstas į ekraną

        print("For the moment, the pool remains available for more work") # išvedamas tekstas į ekraną

    print("Now the pool is closed and no longer available") # išvedamas tekstas į ekraną 