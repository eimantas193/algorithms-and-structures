#Greedy metodas
def recDC(coinValueList,change,knownResults): #godžiojo algoritmo 
   minCoins = change # priskiraima norima iškeisti suma
   if change in coinValueList: # patikrinama ar norima iškeisti suma nėra tarp nominalų
      knownResults[change] = 1 # priskiriamas vienetas gale
      return 1 # gražinamas vienetas
   elif knownResults[change] > 0: # jei paskutinis elementas ne nulis
      return knownResults[change] # išvedamas jo rezultatas
   else: # jei nei viena sąlyga netenkina  
       for i in [c for c in coinValueList if c <= change]: # einama per nominalus kurie yra mažesni arba lygūs ieškomai sumai
         numCoins = 1 + recDC(coinValueList, change-i,
                              knownResults) # naudojama rekursija gauti nuo pirmos vertės siunčiant vis mažesnį skaičių iki pasiekiama iškeitimo vertė 1
         if numCoins < minCoins: # jei norima iškeisti sumos monetų skaičius mažesnis už naują
            minCoins = numCoins # tai priskiraime tą naują kiekį senajam kintamajam
            knownResults[change] = minCoins # priskiriame tarpinę sumą kitamajam tolimesniam atsekimui
   return minCoins # 

print(recDC([1,5,10,25],63,[0]*64)) # į godžiojo metodo funkcija siunčiama sąrašas su nominalais, norima išskladyti suma, bei 64 ilgio nulių sąrašas

print () # 

def dpMakeChange(coinValueList,change,minCoins,coinsUsed): # grąžos gražinimo funkcija kuriai reikia sumos, turimų nominalų, ir dviejų tuščių sąrašų
   for cents in range(change+1): # sukam ciklą kiekvienam centui
      coinCount = cents # priskiriama skaičiavimo suma 
      newCoin = 1 # kitamajam 
      for j in [c for c in coinValueList if c <= cents]: # einama per nominalus kurie yra mažesni arba lygūs ieškomai sumai
            if minCoins[cents-j] + 1 < coinCount: # sąlyga tenkinama jeigu buvusi vertė maženė +1 mažesnė už ieškomą sumą 
               coinCount = minCoins[cents-j]+1 # priskiraima nauja paieškos suma
               newCoin = j # pakeičiama nominalas 
      minCoins[cents] = coinCount # priskiriama naudotas monetų skaičius kiekvienu etapu
      coinsUsed[cents] = newCoin # priskiriamas nominalas naudotas kievienu etapu
   return minCoins[change] # grąžianamas paskutinis narys kuris rodo kiek monetų reiks

def printCoins(coinsUsed,change): # algoritmas skirtas išspausdint naudotus nominalus 
   coin = change # priskirama visa suma kurią norime susumulkint 
   while coin > 0: # ciklas vyksta iki norima susmulkint suma pasiekia nulį
      thisCoin = coinsUsed[coin] # imami nariai iš galo atiminėjant atiminėjant vertes ir gaunant naujos vertės vietą taip iki nelieka keičiamos sumos 
      print(thisCoin) # išspausdina vertę
      coin = coin - thisCoin # atimamas nominalas iš pagrindinės sumos 

def main(): # pagrindinė funkcija
    amnt = 65 # norima išskladyti suma
    clist = [1,5,10,21,25] # turimos monetų nominalai
    coinsUsed = [0]*(amnt+1) # sukuriamas nulių sąrašas
    
    coinCount = [0]*(amnt+1) # sukuriamas nulių sąrašas 

    print("Making change for",amnt,"requires") # išvedamas tekstas į terminalą
    print(dpMakeChange(clist,amnt,coinCount,coinsUsed),"coins") # išvedamas kiekis kiek monetų reikia
    print("They are:") # išvedamas tekstas į terminalą
    printCoins(coinsUsed,amnt) # išvedamos reikalingos monetos
    print("The used list is as follows:") # išvedamas tekstas į terminalą
    print(coinsUsed) # išvedamas sąrašas su naudotu sąrašus apskaičiuoti monetų nominalus ir kiekį

#main() # paleidžia pagrindinę funkciją
