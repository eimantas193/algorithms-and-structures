class Node: # klasė pavadinimu "Node"
    def __init__(self,initdata): # sukuriama pagrindinė finkcija su kitamuoju self ir "initdada" kuris turi būti paduotas kuriant objektą
        self.data = initdata # prie objekto self sukuriamas "data" strukūra su "initdata" duomenimis 
        self.next = None # prie objekto self sukuriamas tuščias objektas "next" su reikšme None 

    def getData(self): # funkcija imanti objektą "self"
        return self.data # grąžina šio objekto vertę

    def getNext(self): # funkcija imanti objektą "self" 
        return self.next # grąžina kintamojo "next" reikšmę (grąžina sekantį objektą)

    def setData(self,newdata): # funkcija imanti objektą "self" ir "newdata", kuris gaunamas kviečiant funkciją 
        self.data = newdata # nauja vertė priskiriami objektui "data" kintamajam 

    def setNext(self,newnext): # funkcija imanti objektą "self" ir "newnext", kuris gaunamas kviečiant funkciją 
        self.next = newnext #  priskiriamas naujas objektas "next" kintamajam 

#temp = Node(93)
#print (temp.getData())

class UnorderedList: # klasė pavadinimu "UnorderedList"

    def __init__(self): # sukuriama pagrindinė funkcija su kitamuoju self
        self.head = None # prie objekto self sukuriamas tuščias masyvas "head" su reikšme None 
    
    def isEmpty(self): # funkcija "isEmpty" imanti objektą "self"
        return self.head == None # grąžina loginį "True" jei obejkto "head" reikšmė lygi "None"
    
    def add(self,item): # funkcija "add" imanti klasę "self" ir "item", kuris turi būti duodamas kviečiant šią funkciją
        temp = Node(item) # sukuriama objektas "temp" pagal klasę Node()
        temp.setNext(self.head) # funkcijai siunčiama senoji vertė (vertė kuri buvo anksčiau įrašyta)
        self.head = temp # priskiriama naujas objektas "head" kintamajam
    
    def size(self): # funkcija "size" imanti objektą "self"
        current = self.head # kintamajam "current" priskiriami duomenys iš klasės "self" "head" kintamojo
        count = 0 # skaitiklis priskiriamas nuliu
        while current != None: # ciklas vyksta iki "current" pasiekiamas tuščias objektas
            count = count + 1 # prie skaitiklio pridedamas + 1
            current = current.getNext() #grąžinamas sekantis objektas 

        return count # grąžinamas esmų verčių kiekis
    
    def search(self,item): # funkcija "search" imanti "self" ir "item", kurio vertė nusprendžiama kviečiant funkciją
        current = self.head # priskiriamas self.head objektas "current" kintamajam
        found = False # kintamajam "found" priskiriama loginis False 
        while current != None and not found: # ciklas sukasi iki reikmė "current" nėra tuščias ir "found" loginė reikšmė nėra "True"
            if current.getData() == item: # sąlyga tenkinama kai nauja vertė yra mažesnė už "item" vertę
                found = True # pakeičiam "stop" vertę į lokinį True
            else: # jei netenkina salygos
                current = current.getNext() # imamas sekantis objektas

        return found # gražina loginę reikšmę "True" jei rado reikšmę, jei ne - "False"
    
    def remove(self,item):
        current = self.head
        previous = None
        found = False
        while not found:
            if current.getData() == item:
                found = True
            else:
                previous = current
                current = current.getNext()

        if previous == None:
            self.head = current.getNext()
        else:
            previous.setNext(current.getNext())

mylist = UnorderedList()

print (mylist.isEmpty())
print (mylist.size())

mylist.add(31)
mylist.add(77)
mylist.add(17)
mylist.add(93)
mylist.add(26)
mylist.add(54)

print (mylist.isEmpty())
print (mylist.size())
print (mylist.search(17))

mylist.remove(17)
print (mylist.search(17))